
{
   keyMul[abi:cxx11](unsigned char const*, unsigned char const*, bool)
   Memcheck:Cond
   fun:slide
   fun:ge_double_scalarmult_vartime
   fun:ge_scalarmult_vartime
   ...
}

{
   ts::TeamSpeakLicense::load(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >&)
   Memcheck:Cond
   fun:slide
   fun:ge_double_scalarmult_vartime
   ...
}

{
   ts::server::VoiceClient::handleCommandClientInitIv(ts::Command&)
   Memcheck:Cond
   fun:mp_mul_2d
   fun:mp_div
   fun:divide
   fun:ecc_sign_hash
   ...
}

{
   keyMul[abi:cxx11](unsigned char const*, unsigned char const*, bool)
   Memcheck:Cond
   ...
   fun:ge_double_scalarmult_vartime
   fun:ge_scalarmult_vartime
   ...
}
{
   call_init
   Memcheck:Leak
   match-leak-kinds: possible
   fun:calloc
   fun:g_malloc0
   obj:/usr/lib/x86_64-linux-gnu/libgobject-2.0.so.0.5600.3
   fun:g_type_register_fundamental
   obj:/usr/lib/x86_64-linux-gnu/libgobject-2.0.so.0.5600.3
   obj:/usr/lib/x86_64-linux-gnu/libgobject-2.0.so.0.5600.3
   fun:call_init
   fun:_dl_init
   obj:/lib/x86_64-linux-gnu/ld-2.27.so
}

{
   call_init
   Memcheck:Leak
   match-leak-kinds: possible
   fun:realloc
   fun:g_realloc
   obj:/usr/lib/x86_64-linux-gnu/libgobject-2.0.so.0.5600.3
   fun:g_type_register_static
   fun:g_param_type_register_static
   obj:/usr/lib/x86_64-linux-gnu/libgobject-2.0.so.0.5600.3
   obj:/usr/lib/x86_64-linux-gnu/libgobject-2.0.so.0.5600.3
   fun:call_init
   fun:_dl_init
   obj:/lib/x86_64-linux-gnu/ld-2.27.so
}

{
   call_init
   Memcheck:Leak
   match-leak-kinds: reachable
   fun:malloc
   fun:realloc
   fun:g_realloc
   obj:/usr/lib/x86_64-linux-gnu/libgobject-2.0.so.0.5600.3
   fun:g_type_register_fundamental
   obj:/usr/lib/x86_64-linux-gnu/libgobject-2.0.so.0.5600.3
   obj:/usr/lib/x86_64-linux-gnu/libgobject-2.0.so.0.5600.3
   fun:call_init
   fun:_dl_init
   obj:/lib/x86_64-linux-gnu/ld-2.27.so
}

{
   call_init
   Memcheck:Leak
   match-leak-kinds: possible
   fun:malloc
   fun:realloc
   fun:g_realloc
   obj:/usr/lib/x86_64-linux-gnu/libgobject-2.0.so.0.5600.3
   fun:g_type_register_fundamental
   obj:/usr/lib/x86_64-linux-gnu/libgobject-2.0.so.0.5600.3
   obj:/usr/lib/x86_64-linux-gnu/libgobject-2.0.so.0.5600.3
   fun:call_init
   fun:_dl_init
   obj:/lib/x86_64-linux-gnu/ld-2.27.so
}

{
   call_init
   Memcheck:Leak
   match-leak-kinds: possible
   fun:malloc
   fun:realloc
   fun:g_realloc
   obj:/usr/lib/x86_64-linux-gnu/libgobject-2.0.so.0.5600.3
   fun:g_type_register_fundamental
   obj:/usr/lib/x86_64-linux-gnu/libgobject-2.0.so.0.5600.3
   obj:/usr/lib/x86_64-linux-gnu/libgobject-2.0.so.0.5600.3
   fun:call_init
   fun:_dl_init
   obj:/lib/x86_64-linux-gnu/ld-2.27.so
}
