#include <algorithm>
#include <utility>
#include <log/LogUtils.h>
#include "VirtualServerManager.h"
#include "src/server/VoiceServer.h"
#include "InstanceHandler.h"

using namespace std;
using namespace ts;
using namespace ts::server;

#define PREFIX string("[SNAPSHOT] ")
